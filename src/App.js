// 导入库
import React, { Component } from 'react'

// 导入样式
import './static/css/reset.less';
import {AppDiv,MusicDetail} from './styled'

// 导入路由
import Router from './router/index'

// 导入utils
import './utils/rem.js'  

// 定义组件
class App extends Component  
{
 render() {
     return (
         <AppDiv>
             <Router />

             {/* <MusicDetail>
                 <img className="bg" src="https://y.gtimg.cn/music/photo_new/T002R300x300M000001QmQGO4e1nSW.jpg?max_age=2592000" alt="bg" />
                 <img className="cd cdrotate" src="https://y.gtimg.cn/music/photo_new/T002R300x300M000001QmQGO4e1nSW.jpg?max_age=2592000" alt="cd" />

               <div className="play">
                <audio ref="audio" controls src="http://aqqmusic.tc.qq.com/amobile.music.tc.qq.com/C40000189mAI2BxJrT.m4a?guid=6111131940&vkey=7BB4C9D84F23169CFF61D1EBA6E001F08BFCED427ECC6347AC8F7A68A4C211CF1E9E6401207F055BD12B6F89D32CC5E8D131A2436DEB78F3&uin=0&fromtag=38">
                    </audio>

                    <button onClick={() => {
                        this.refs.audio.play()
                    }}>播放</button>

                    
                    <button onClick={() => {
                        this.refs.audio.pause()
                    }}>暂停</button>

                    
                    <button onClick={() => {
                        this.refs.audio.pause()
                        this.refs.audio.src = 'http://aqqmusic.tc.qq.com/amobile.music.tc.qq.com/C4000024g7CS1NpWNd.m4a?guid=6111131940&vkey=C3520894B71B70BCEEFDF69400609E06EE9DBF6A4C77039269D54D30045B8EC776273A2287971E5918F3AF29C50B1890B8ED185781C25582&uin=0&fromtag=38'
                        this.refs.audio.play()
                    }}>换歌</button>

                    
                    <button onClick={() => {
                        this.refs.audio.currentTime = 30
                    }}>拖拽指定事件播放</button>
               </div>
             </MusicDetail> */}
         </AppDiv>
     )
 }
}

export default App