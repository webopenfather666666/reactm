// 导入库
import React, { Component } from 'react'

// 导入样式
import {LbtDiv} from './styled'

import Swiper from 'swiper/js/swiper.js'
import 'swiper/css/swiper.min.css'

class Lbt extends Component
{
    render() {
        return (
            <LbtDiv>
                <div className="swiper-container">
                    <div className="swiper-wrapper">
                        <div className="swiper-slide">Slide 1</div>
                        <div className="swiper-slide">Slide 2</div>
                        <div className="swiper-slide">Slide 3</div>
                        <div className="swiper-slide">Slide 4</div>
                    </div>
                    <div className="swiper-pagination"></div>
                </div>
            </LbtDiv>
        )
    } 
 
    componentDidMount()
    {
        new Swiper ('.swiper-container', {
            loop: true,  //循环
            autoplay: {   //滑动后继续播放（不写官方默认暂停）
                disableOnInteraction: false,
            },
            pagination: {  //分页器
                el: '.swiper-pagination'
            }
        })
    }
} 

export default Lbt