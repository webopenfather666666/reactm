import styled from 'styled-components'

export const LbtDiv = styled.div`
    width: 100%;
    height: 1.5rem;
    background: red;

    .swiper-container {
        width: 100%;
        height: 1.5rem;
    }  

    .swiper-pagination {
        bottom: -.16rem
    }
`