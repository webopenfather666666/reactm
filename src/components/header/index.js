import React, { Component } from 'react'

import {HeaderDiv} from './styled'


class HeaderIndex extends Component {
 render() {
     return (
         <HeaderDiv>
             <img src="/logo192.png" alt="logo" />
             <span>神龙吟</span>
         </HeaderDiv>
     )
 } 
}

export default HeaderIndex