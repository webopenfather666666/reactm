import React, { Component } from 'react'

import {GdyDiv} from './styled'

import "animate.css";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";


class Gdy extends Component 
{

 render() {
     
    //  <GdyDiv style={ {display:'none'} }>
     return (
        <ReactCSSTransitionGroup
                transitionEnter={true}
                transitionLeave={true}
                transitionEnterTimeout={2500}
                transitionLeaveTimeout={1500}
                transitionName="animated"
        >
         <GdyDiv  className="animated fadeInRightBig" style={ this.props.status ? {display:'block'} : {display:'none'} }>
            <button onClick={() =>{
                this.props.getGdyParamsFn()
            }}>关闭</button>
         </GdyDiv>
         </ReactCSSTransitionGroup>
     )
 } 
}

export default Gdy