import styled from 'styled-components'

export const NavsDiv = styled.div`
    width: 100%;
    height: 44px;
    display: flex;

    a {
        display: inline-block;
        width: 25%;
        height: 44px;
        font-size: 18px;
        color: hsla(0,0%,100%,.5);
        display: flex;
        justify-content: center;
        align-items: center;
    }

    a.active { color: #ffcd32;  }
    a.active span{ border-bottom:2px solid #ffcd32 }
`