import styled from 'styled-components'

export const AppDiv = styled.div`
    font-size: 14px;
    width: 100%;
    height: 100%;
    background: #222;
    overflow: hidden;
`

export const MusicDetail = styled.div`
    width:100%;
    height: 100%;
    display:flex;
    justify-content:center;
    flex-direction: column;


    .bg {
        width:100%;
        height: 100%;
        position: absolute;
        left:0px;
        top: 0px;
        z-index: 9;
        filter:blur(20px);
    }

    .play {
        position: relative;
        z-index: 10;
    }

    .cd {
        position: relative;
        z-index: 10;
        width: 2.87rem;
        height: 2.87rem;
        box-sizing: border-box;
        border-radius: 50%;
        border: 10px solid hsla(0,0%,100%,.1);
    }

    .cdrotate {
        animation: imgrotate 6s linear infinite;}
    }

    @keyframes imgrotate
    {       
        0%{transform:rotate(0deg);}
        50%{transform:rotate(180deg);}
        100%{transform:rotate(360deg);}
    }
`