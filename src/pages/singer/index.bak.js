import React, { Component } from 'react'

// 导入样式
import {SingerDiv} from './styled'

// 导入组件
import Header from '../../components/header'
import Navs from '../../components/navs'

// 导入第三方模块
import BScroll from 'better-scroll'

class Singer extends Component 
{
    constructor(props)
    {
        super(props)

        this.state = {

            index: ['热', 'A', 'B', 'C'],
            currentIndex: 0,
            singers: [
                {title: "推荐", starts: [
                    {name:"神龙教主1", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主2", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主3", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主4", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主5", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主6", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                ]},
                {"title": "A", starts: [
                    {name:"神龙教主1", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主2", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主3", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主4", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主5", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主6", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                ]},
                {"title": "B", starts: [
                    {name:"神龙教主1", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主2", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主3", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主4", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主5", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主6", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                ]},
                {"title": "C", starts: [
                    {name:"神龙教主1", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主2", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主3", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主4", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主5", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主6", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                ]},
            ]
        }
    }

    

    componentDidMount()
    {
        let wrapper = document.querySelector('.singers')
        let scroll = new BScroll(wrapper, {
            probeType:3
            // 不需要上拉刷新
            // pullUpLoad: {
            //     threshold: 50
            // }
        })

        // 需要监控滚动
        scroll.on('scroll', (pos) => {
            // let x = pos.x
            let y = Math.abs(pos.y)
            // console.log('你滚动了', x,y)
            console.log(this.refs) // VUE中：this.$refs

            // 遍历获取offsetTop坐标 和 y对比
            for (let key in this.refs)
            {   
                // console.log(key, this.refs.键)
                // 但是键是变量因此换语法：对象[变量名]
                // console.log(key, this.refs[key])
                // console.log(key, this.refs[key].offsetTop)
                // index.js:85 推荐 0
                // index.js:85 A 450
                // index.js:85 B 900
                // index.js:85 C 1350

                let contentY = this.refs[key].offsetTop

                // 假如 滚动条坐标 >= 当前坐标
                //      滚动条坐标 > 热  
                //      y >=  this.refs[key].offsetTop
                // 问题：除了热本身其他都大于 哪是A、B、C

                // 解决： 滚动条坐标 >= 当前坐标 && 滚动条坐标 <下一个坐标
                // 举例：   滚动条坐标 >= 热 && 滚动条坐标 < B  这样你才算A

                // 成立：this.setState({currentIndex: 下标})
                
                // if (y >= contentY && y < 下一个坐标) {

                // }
            }

            // 目的：改变 currentIndex = ?数字
            // 已知：y滚动坐标
            // 缺少：对比对象 （页面中A、B、C距顶部举例
        })

        // 不需要监控上拉
        // scroll.on('pullingUp', () => {
        //     console.log('触发上拉刷新啦~~')
        //         // 3.切记切记切记：操作完成之后，一定要说一下上拉完毕
        //         // scroll.finishPullUp()
        // })
    }
    
 render() {
     return (
         <SingerDiv>
            <Header />
            <Navs />
             {/* this is Singer index */}

            <ul className="singerIndex">
                {this.state.index.map((item,index) => {
                    return <li key={index} className={this.state.currentIndex === index ? 'active': ''}>{item}</li>
                })}
            </ul>

            {/* 宽度100%，高度剩余空间 超出部分隐藏 */}
            <div className="singers">
                <div>
                    {/* <div className="singer">
                        <h2>热门</h2>
                        <ul>
                            <li>
                                <img src="" alt="" />
                                <span>周杰伦</span>
                            </li>
                        </ul>
                    </div> */}

                    {this.state.singers.map((item,index)=> {
                        return <div ref={item.title} key={index} className="singer">
                            <h2>{item.title}</h2>
                            <ul>
                                { 
                                    item.starts.map((item, index) => {
                                        return <li key={index}>
                                            <img src={item.img} alt="avatar" />
                                            <span>{item.name}</span>
                                        </li>
                                    })
                                }
                            </ul>
                        </div>
                    })}
                </div>
            </div>
         </SingerDiv>  
     )
 }
}

export default Singer