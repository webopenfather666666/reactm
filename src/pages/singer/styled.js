import styled from 'styled-components'


export const SingerDiv = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    
    .singerIndex {
        width: 20px;
        padding: 20px 0;
        border-radius: 10px;
        text-align: center;
        background: rgba(0,0,0,.3);
        font-family: Helvetica;
        position: fixed;
        right: 0px;
        z-index:2;
        top: 50%;
        transform: translateY(-50%);
        
        li {
            width: 100%;
            text-align: center;
            color: hsla(0,0%,100%,.5);
            font-size: 12px;
            padding: 5px 0px;
        }

        li.active {color: #ffcd32;}
    }

    .singers {
        width:100%;
        flex:1;
        overflow: hidden;
        background: green;

        h2 {
            width: 100%;
            height: 30px;
            line-height: 30px;
            // 下面代码是我直接参考网站的
            // 后期你实战用蓝湖
            // 也可以直接在右下角复制
            padding-left: 20px;
            font-size: 12px;
            color: hsla(0,0%,100%,.5);
            background: #333;
        }

        ul {
            width: 90%;
            margin: auto;
            background: red;

            li {
                width: 100%;
                height: 70px;
                display: flex;
                align-items: center;
            }

            li img {
                width: 50px;
                height: 50px;
                border-radius: 50%;
            }
    
            li span {
                margin-left: 20px;
                color: hsla(0,0%,100%,.5);
                font-size: 14px;
            }
        }
    }
`