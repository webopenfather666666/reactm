import React, { Component } from 'react'

// 导入样式
import {SingerDiv} from './styled'

// 导入组件
import Header from '../../components/header'
import Navs from '../../components/navs'
import Gdy from '../../components/gdy'

// 导入第三方模块
import BScroll from 'better-scroll'
class Singer extends Component 
{
    constructor(props)
    {
        super(props)

        this.state = {
            scroll:null,
            isShowGdy:false,
            index: ['热', 'A', 'B', 'C'],
            currentIndex: 0,
            singers: [
                {title: "推荐", starts: [
                    {name:"神龙教主1", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主2", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主3", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主4", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主5", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主6", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                ]},
                {"title": "A", starts: [
                    {name:"神龙教主1", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主2", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主3", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主4", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主5", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主6", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                ]},
                {"title": "B", starts: [
                    {name:"神龙教主1", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主2", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主3", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主4", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主5", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主6", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                ]},
                {"title": "C", starts: [
                    {name:"神龙教主1", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主2", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主3", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主4", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主5", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主6", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主6", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主6", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主6", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主6", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主6", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                    {name:"神龙教主6", img: "https://y.gtimg.cn/music/photo_new/T001R300x300M000003ArN8Z0WpjTz.jpg?max_age=2592000"},
                ]},
            ]
        }
    }

    

    componentDidMount()
    {
        let wrapper = document.querySelector('.singers')
        let scroll = new BScroll(wrapper, {
            probeType:3
            // 不需要上拉刷新
            // pullUpLoad: {
            //     threshold: 50
            // }
        })
        this.setState({scroll})

        // 数据过滤
        let h2ScrollTop = []
        // console.log(this.refs)  // offsetTop 对象
        for (let key in this.refs)
        {
            // console(key, this.refs[key])
            // key 下标：推荐、A、B、C..
            //  this.refs[key] 标签对象  我们需要里面的 offsetTop
            // console.log(this.refs[key].offsetTop)

            // 留心：为什么要减少88
            // 因为：y 当前滚动条坐标是从0开始计算的
            // 然后：我希望h2坐标也从0开始计算 
            // 因此：减去头部举例
            h2ScrollTop.push(this.refs[key].offsetTop - 88)
        }
        //  [0, 450, 900, 1350] 绝对是由小到大
        console.log(h2ScrollTop)

        // 需要监控滚动
        scroll.on('scroll', (pos) => {
            // console.log('你滚动了', pos.y)

            // 分析
            // 当前滚动条位置：通过监控滚动 然后形参对象中的y属性可以获取
            // 默认推荐、A、B距离顶部位置：1-给他们加ref属性
            //                          2-通过打印this.refs可以获取
            // 准备：先把默认显示h2距离顶部的位置 过滤成方便滚动的时候操作

            // 步骤1：准备当前滚动条坐标 （留心：绝对值 因为有负数
            let y = Math.abs(pos.y)

            // 步骤2：准备h2所有距离顶部的坐标（咱们上面已经准备好了  原来是对象不方便操作 过滤成数组
            // h2ScrollTop

            // 步骤3：检测 y 和 h2ScrollTop [0, 450, 900, 1350]  哪条数据匹配
            // 3.1 挨个输出对比   
            // 3.2 更新 currentIndex
            // 明确：
            // 匹配0这个数据   那就是推荐选中
            // 匹配450这个数据 那就是A选中
            // ..  
            // 思考：现在坐标是600应该让谁选中
            // 回答：应该A  900之后让B选中
            // 思考：先和小的比 还是 大的
            // 回答：先和大的比   y > h2ScrollTop这里面数字就直接选中  break退出
            // 因为：假设现在y=1600  最大的一条都符合了 前面就不用考虑了
            for (let i=h2ScrollTop.length-1; i>=0; i--)
            {
                // console.log(h2ScrollTop[i]) 

                // 让当前滚动条坐标
                // 和h2距离顶部坐标挨个比（从大的比
                // 只要成立 直接退出
                if (y >= h2ScrollTop[i])
                {
                    this.setState({
                        currentIndex: i
                    })
                    break;
                }
            }

        })

        // 不需要监控上拉
        // scroll.on('pullingUp', () => {
        //     console.log('触发上拉刷新啦~~')
        //         // 3.切记切记切记：操作完成之后，一定要说一下上拉完毕
        //         // scroll.finishPullUp()
        // })
    }

  goListData(item)
    {
        console.log(item)
        // scroll对象.scrollToElement(索引栏对应的列表部分节点)
        // scroll对象：默认componentDidMount里面创建的但是这边获取不到
        // 因此：创建好了之后保存到state中 我就可以在这里使用了
        // 索引栏对应的列表部分节点：this.refs 对象  键就是热 A、B
        item = item === '热' ? '推荐' : item
        // this.state.scroll.scrollToElement(this.refs[item])
        this.state.scroll.scrollToElement(this.refs[item], 1000)

    }
    
    showGdyFn() 
    {
        // alert(1)
        this.setState({
            isShowGdy: true
        })
    }

    // getGdyParamsFn() 
    getGdyParamsFn = () => {
        this.setState({
            isShowGdy: false
        })
    }

 render() {
     return (
         <SingerDiv>
             {/* 歌单页 */}
            {/* <Gdy status={false}/> */}
            <Gdy key="amache"  status={this.state.isShowGdy} getGdyParamsFn={this.getGdyParamsFn}/>
            
            <Header />
            <Navs />
             {/* this is Singer index */}

            <ul className="singerIndex">
                {this.state.index.map((item,index) => {
                    return <li onClick={this.goListData.bind(this,item)} key={index} className={this.state.currentIndex === index ? 'active': ''}>{item}</li>
                })}
            </ul>

            {/* 宽度100%，高度剩余空间 超出部分隐藏 */}
            <div className="singers">
                <div>
                    {/* <div className="singer">
                        <h2>热门</h2>
                        <ul>
                            <li>
                                <img src="" alt="" />
                                <span>周杰伦</span>
                            </li>
                        </ul>
                    </div> */}

                    {this.state.singers.map((item,index)=> {
                        return <div ref={item.title} key={index} className="singer">
                            <h2>{item.title}</h2>
                            <ul>
                                { 
                                    // TODO. 晚上发笔记的时候解决 
                                    item.starts.map((item, index) => {
                                        return <li 
                                            key={index}
                                            onClick={this.showGdyFn.bind(this)}  
                                        >
                                            <img src={item.img} alt="avatar" />
                                            <span>{item.name} 666</span>
                                        </li>
                                    })
                                }
                            </ul>
                        </div>
                    })}
                </div>
            </div>
         </SingerDiv>  
     )
 }
}

export default Singer