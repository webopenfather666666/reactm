import styled from 'styled-components'

export const RecommendDiv = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;

    .wrapper {
        width: 85%;
        margin: auto;
        flex: 1;
        background: green;
        overflow: hidden
    }

    ul {
        li {
            display: flex;
            padding-bottom: 20px;

            .l {
                display: flex;
                width: 80px;
                height: 63px;
                img {width: 60px; height: 60px;}
            }

            .r {
                .title { font-size: 18px;margin-top: 5px;}
                .desc { font-size: 16px; margin-top: 8px;}
            }
        }
    }
`