// 导入模块
import React, { Component } from 'react'

// 导入样式
import {RecommendDiv} from './styled'

// 导入组件
import Header from '../../components/header'
import Navs from '../../components/navs'
import Lbt from '../../components/lbt'

// 导入第三方模块
import BScroll from 'better-scroll' 
import axios from 'axios'
 

class Recommend extends Component 
{
    
    constructor(props)
    {
        super(props)

        this.state = {
            pagenum: 1,
            musics: [
                {
                    img: 'http://p.qpic.cn/music_cover/jF1CHiathfGvgNZrwbCZkzs3HNH3GZPHcx6nvzQFnkd6XcY3zItIySA/600?n=1',
                    title: '测试',
                    desc: '练习'
                },
                {
                    img: 'http://p.qpic.cn/music_cover/jF1CHiathfGvgNZrwbCZkzs3HNH3GZPHcx6nvzQFnkd6XcY3zItIySA/600?n=1',
                    title: '测试',
                    desc: '练习'
                },
                {
                    img: 'http://p.qpic.cn/music_cover/jF1CHiathfGvgNZrwbCZkzs3HNH3GZPHcx6nvzQFnkd6XcY3zItIySA/600?n=1',
                    title: '测试',
                    desc: '练习'
                },
                {
                    img: 'http://p.qpic.cn/music_cover/jF1CHiathfGvgNZrwbCZkzs3HNH3GZPHcx6nvzQFnkd6XcY3zItIySA/600?n=1',
                    title: '测试',
                    desc: '练习'
                },
                {
                    img: 'http://p.qpic.cn/music_cover/jF1CHiathfGvgNZrwbCZkzs3HNH3GZPHcx6nvzQFnkd6XcY3zItIySA/600?n=1',
                    title: '测试',
                    desc: '练习'
                },
                {
                    img: 'http://p.qpic.cn/music_cover/jF1CHiathfGvgNZrwbCZkzs3HNH3GZPHcx6nvzQFnkd6XcY3zItIySA/600?n=1',
                    title: '测试',
                    desc: '练习'
                },
                {
                    img: 'http://p.qpic.cn/music_cover/jF1CHiathfGvgNZrwbCZkzs3HNH3GZPHcx6nvzQFnkd6XcY3zItIySA/600?n=1',
                    title: '测试',
                    desc: '练习'
                },
                {
                    img: 'http://p.qpic.cn/music_cover/jF1CHiathfGvgNZrwbCZkzs3HNH3GZPHcx6nvzQFnkd6XcY3zItIySA/600?n=1',
                    title: '测试',
                    desc: '练习'
                },
            ]
        }
    }

    componentDidMount()
    {
        
        // 注意：逻辑上要结合redux
        // 但是：现在简化流程让你学会处理跨域
        // 因此：直接导入axios发送异步请求
        axios.get('/api/music/api/getTopBanner?g_tk=1928093487&inCharset=utf8&outCharset=utf-8&notice=0&format=json&platform=yqq.json&hostUin=0&needNewCode=0&-=recom6008162838079509&data=%7B%22comm%22:%7B%22ct%22:24%7D,%22category%22:%7B%22method%22:%22get_hot_category%22,%22param%22:%7B%22qq%22:%22%22%7D,%22module%22:%22music.web_category_svr%22%7D,%22recomPlaylist%22:%7B%22method%22:%22get_hot_recommend%22,%22param%22:%7B%22async%22:1,%22cmd%22:2%7D,%22module%22:%22playlist.HotRecommendServer%22%7D,%22playlist%22:%7B%22method%22:%22get_playlist_by_category%22,%22param%22:%7B%22id%22:8,%22curPage%22:1,%22size%22:40,%22order%22:5,%22titleid%22:8%7D,%22module%22:%22playlist.PlayListPlazaServer%22%7D,%22new_song%22:%7B%22module%22:%22newsong.NewSongServer%22,%22method%22:%22get_new_song_info%22,%22param%22:%7B%22type%22:5%7D%7D,%22new_album%22:%7B%22module%22:%22newalbum.NewAlbumServer%22,%22method%22:%22get_new_album_info%22,%22param%22:%7B%22area%22:1,%22sin%22:0,%22num%22:10%7D%7D,%22new_album_tag%22:%7B%22module%22:%22newalbum.NewAlbumServer%22,%22method%22:%22get_new_album_area%22,%22param%22:%7B%7D%7D,%22toplist%22:%7B%22module%22:%22musicToplist.ToplistInfoServer%22,%22method%22:%22GetAll%22,%22param%22:%7B%7D%7D,%22focus%22:%7B%22module%22:%22QQMusic.MusichallServer%22,%22method%22:%22GetFocus%22,%22param%22:%7B%7D%7D%7D')
        .then(res => {
            console.log(111)
            console.log(res)
            console.log(2222)
        })
        
        let wrapper = document.querySelector('.wrapper')
        let scroll = new BScroll(wrapper, {
            pullUpLoad: {
                threshold: 50
            }
        })
        // 监控
        scroll.on('pullingUp', () => {
            console.log('触发上拉刷新啦~~')

            let pagenum = this.state.pagenum
            let musics = this.state.musics
            let newMusics = musics.concat([
                {
                    img: 'http://p.qpic.cn/music_cover/jF1CHiathfGvgNZrwbCZkzs3HNH3GZPHcx6nvzQFnkd6XcY3zItIySA/600?n=1',
                    title: '测试',
                    desc: '练习'
                },
                {
                    img: 'http://p.qpic.cn/music_cover/jF1CHiathfGvgNZrwbCZkzs3HNH3GZPHcx6nvzQFnkd6XcY3zItIySA/600?n=1',
                    title: '测试',
                    desc: '练习'
                },
                {
                    img: 'http://p.qpic.cn/music_cover/jF1CHiathfGvgNZrwbCZkzs3HNH3GZPHcx6nvzQFnkd6XcY3zItIySA/600?n=1',
                    title: '测试',
                    desc: '练习'
                },
                {
                    img: 'http://p.qpic.cn/music_cover/jF1CHiathfGvgNZrwbCZkzs3HNH3GZPHcx6nvzQFnkd6XcY3zItIySA/600?n=1',
                    title: '测试',
                    desc: '练习'
                }
            ])

            console.log(`第${pagenum}页`)

            this.setState({
                musics: newMusics,
                pagenum: pagenum+1
            }, () => {
    
                // 3.切记切记切记：操作完成之后，一定要说一下上拉完毕
                scroll.finishPullUp()
            })

        })
    }

    render() 
    {
     return (
         <RecommendDiv>
             <Header />
             <Navs />
             <Lbt />
             
             <div className="wrapper">
                <ul>
                    {/* <li>
                        <div className="l">
                            <img src="http://p.qpic.cn/music_cover/jF1CHiathfGvgNZrwbCZkzs3HNH3GZPHcx6nvzQFnkd6XcY3zItIySA/600?n=1" />
                        </div>
                        <div className="r">
                            <p className="title">神龙教主</p>
                            <p className="desc" title="">绝对刺缴!早读</p>
                        </div>
                    </li> */}

                    {
                        this.state.musics.map((item,index) => {
                            return <li key={index}>
                                <div className="l">
                                    <img src={item.img} alt="item.title"/>
                                </div>
                                <div className="r">
                                    <p className="title">{item.title}</p>
                                    <p className="desc" title="">{item.desc}</p>
                                </div>
                            </li>
                        })
                    }
                </ul>
             </div>
             
{/*
    // <div>
    //     <img src="http://hbimg.b0.upaiyun.com/8c992f2b280b3f103cc60a5e32b6fc99b6ba801b6580-x6s670_fw658" alt="loading"/>
    // </div>
*/}

         </RecommendDiv>
     )
 }

}

export default Recommend