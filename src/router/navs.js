// 导入组件
import Recommend from '../pages/recommend';
import Singer from '../pages/singer';
import Rank from '../pages/rank';
import Search from '../pages/search';
import Antdm from '../pages/antdm';

// 导出路由
export default [
 {
     path: '/recommend',
     title: '推荐',
     exact:true,
     component: Recommend,
 },
 {
     path: '/singer',
     title: '歌手',
     component: Singer,
     islogin: false
 },
 {
     path: '/rank',
     title: '排行',
     component: Rank,
     islogin: false
 },
 {
     path: '/search',
     title: '搜索',
     component: Search
 },
 {
     path: '/antdm',
     title: '使用antdm布局',
     component: Antdm
 }
]