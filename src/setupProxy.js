const proxy = require('http-proxy-middleware')

module.exports = function(app) {

  // 明确：http://ustbhuangyi.com/music/api/getTopBanner?g_tk=1928093487&in
  // 组成：【请求网址            】【请求路径           】【请求参数
  // 1. 将axios里面的【请求网址】放到【下面target中】
  // 2. 将【proxy函数第一个参数里面的值】写到axios【请求网址中】
  // 就OK
  // 目的：这样后期检测到请求路径时/api开头的就会拦截 代理请求
  // 留心1：proxy函数第一个参数你叫/api叫/api2都行随意
  // 留心2：最终的请求地址 【target地址+axios里面写的地址】
  // http://ustbhuangyi.com/api/music/api/getTopBanner?g_tk=1928093487&inCharset=utf8&outC.....
  // 细心的小伙伴会发现这样的多了一个api地址
  // 解决：通过配置pathRewrite去掉
  // http://ustbhuangyi.com/music/api/getTopBanner?g_tk=1928093487&inCharset=utf8&outC.....
  app.use(
  	proxy('/api', {
       target: "http://ustbhuangyi.com", // 目标服务器网址
       changeOrigin: true, // 是否允许跨域
       secure: false,      // 关闭SSL证书验证HTTPS接口需要
       pathRewrite: {      // 重写路径请求
        '^/api' : ''
        // 重写
        // pathRewrite: {'^/old/api' : '/new/api'}
        // 移除
        // pathRewrite: {'^/remove/api' : ''}
        // 添加
        // pathRewrite: {'^/' : '/basepath/'}
       },
    })
  );

  // ...
  
};