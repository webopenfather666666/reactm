import React from 'react';
import logo from './logo.svg';
import './App.css';

import {
  AppDiv
} from './styled'
import styled from './App.module.css'
import styledLess from './App.module.less'

import { Button, WhiteSpace } from 'antd-mobile';

function App() {
  return (
    <AppDiv>
    <div className="App">

      <ul className={styledLess.ul}>
        <li>111</li>
        <li>222</li>
        <li>333</li>
      </ul>

      <h1 className={styledLess.t1}>测试LESS模块化</h1>
      <h1 className={styled.h1}>测试CSS默认模块化</h1>
      <hr />
    
      <Button>default</Button><WhiteSpace />
      <Button disabled>default disabled</Button><WhiteSpace />

      <Button type="primary">primary</Button><WhiteSpace />
      <Button type="primary" disabled>primary disabled</Button><WhiteSpace />

      <Button type="warning">warning</Button><WhiteSpace />
      <Button type="warning" disabled>warning disabled</Button><WhiteSpace />
      
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
    </AppDiv>
  );
}

export default App;
